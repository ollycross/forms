<?php

namespace OllyOllyOlly\Forms;

use Nette\Forms\Controls;
use Nette\Forms\Rendering\DefaultFormRenderer;
use Nette\Utils\Html;

class BootstrapFormRenderer extends DefaultFormRenderer
{
    protected $_doingColumn = false;
    protected $_doingRow = false;

    /*
        $renderer->wrappers['control']['container'] = 'div';
        $renderer->wrappers['label']['container'] = 'div class="control-label"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
*/
    public $wrappers = [
        'form' => [
            'container' => null,
        ],

        'error' => [
            'container' => 'ul class="list-unstyled alert alert-danger"',
            'item' => 'li',
        ],

        'group' => [
            'container' => 'fieldset',
            'label' => 'legend',
            'description' => 'p',
        ],

        'controls' => [
            'container' => null,
        ],

        'pair' => [
            'container' => 'div class=form-group',
            '.required' => 'required',
            '.optional' => null,
            '.odd' => null,
            '.error' => 'has-error',
        ],

        'control' => [
            'container' => null,
            '.odd' => null,

            'description' => 'p class=help-block',
            'requiredsuffix' => '',
            'errorcontainer' => 'span class=help-block',
            'erroritem' => '',

            '.required' => 'required',
            '.text' => 'text',
            '.password' => 'text',
            '.file' => 'text',
            '.email' => 'text',
            '.number' => 'text',
            '.submit' => 'button',
            '.image' => 'imagebutton',
            '.button' => 'button',
        ],

        'label' => [
            'container' => null,
            'suffix' => null,
            'requiredsuffix' => '',
        ],

        'hidden' => [
            'container' => null,
        ],
    ];
    public function render(\Nette\Forms\Form $form, $mode = null)
    {
        $usedPrimary = false;
        foreach ($form->getControls() as $control) {
            if ($control instanceof Controls\Button) {
                if (strpos($control->getControlPrototype()->getAttribute('class'), 'btn') === false) {
                    $control->getControlPrototype()->addClass($usedPrimary ? 'btn btn-primary' : 'btn btn-primary');
                    $usedPrimary = true;
                }
            } elseif ($control instanceof Controls\TextBase || $control instanceof Controls\SelectBox || $control instanceof Controls\MultiSelectBox) {
                $control->getControlPrototype()->addClass('form-control');
            } elseif ($control instanceof Controls\Checkbox || $control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList) {
                $control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
            }
        }

        return parent::render($form, $mode);
    }

    protected function _addRow()
    {
        $this->_doingRow = true;

        return '<div class="row">';
    }
    protected function _endRow()
    {
        $this->_doingRow = false;

        return '</div>';
    }
    protected function _addColumn($span)
    {
        $this->_doingColumn = true;

        return "<div class=\"col-sm-{$span}\">";
    }
    protected function _endColumn()
    {
        $this->_doingColumn = false;

        return '</div>';
    }

    public function renderBody()
    {
        $s = $remains = '';

        $defaultContainer = $this->getWrapper('group container');
        $translator = $this->form->getTranslator();
        $columns = $this->form->getColumns();
        $rows = $this->form->getRows();
        $gIx = 0;
        foreach ($this->form->getGroups() as $group) {
            if (isset($rows[$gIx])) {
                $s .= $this->_addRow();
            }
            if (isset($columns[$gIx])) {
                if (!$this->_doingRow) {
                    $s .= $this->_addRow();
                }
                $s .= $this->_addColumn($columns[$gIx]);
            }
            if (!$group->getControls() || !$group->getOption('visual')) {
                continue;
            }

            $container = $group->getOption('container', $defaultContainer);
            $container = $container instanceof Html ? clone $container : Html::el($container);
            $class = $group->getOption('class');
            if ($class) {
                $container->class = $class;
            }

            $id = $group->getOption('id');
            if ($id) {
                $container->id = $id;
            }

            $s .= "\n".$container->startTag();

            $text = $group->getOption('label');
            if ($text instanceof Html) {
                $s .= $this->getWrapper('group label')->addHtml($text);
            } elseif (is_string($text)) {
                if ($translator !== null) {
                    $text = $translator->translate($text);
                }
                $s .= "\n".$this->getWrapper('group label')->setText($text)."\n";
            }

            $text = $group->getOption('description');
            if ($text instanceof Html) {
                $s .= $text;
            } elseif (is_string($text)) {
                if ($translator !== null) {
                    $text = $translator->translate($text);
                }
                $s .= $this->getWrapper('group description')->setText($text)."\n";
            }

            $s .= $this->renderControls($group);

            $remains = $container->endTag()."\n".$remains;
            if (!$group->getOption('embedNext')) {
                $s .= $remains;
                $remains = '';
            }
            if (isset($columns[$gIx + 1])) {
                $s .= $this->_endColumn();
            }
            if (isset($rows[$gIx + 1])) {
                $s .= $this->_endRow();
            }
            ++$gIx;
        }
        if ($this->_doingColumn) {
            $s .= $this->_endColumn();
        }
        if ($this->_doingRow) {
            $s .= $this->_endRow();
        }

        $s .= $remains.$this->renderControls($this->form);

        $container = $this->getWrapper('form container');
        $container->setHtml($s);

        return $container->render(0);
    }
}
