<?php

namespace OllyOllyOlly\Forms\Http;

use Nette\Http\Request;
use Nette\Http;

/**
 * Current HTTP request factory.
 */
class RequestFactory extends Http\RequestFactory
{
    private $_rawBodyCallback;

    protected function _getRequestContentType()
    {
        return;
    }

    public function rawBodyCallback()
    {
        $f3 = \Base::instance();
        $raw = $f3->get('BODY');
        switch (explode(';', $f3->get('SERVER.CONTENT_TYPE'))[0]) {
            case 'application/x-www-form-urlencoded':
                parse_str($raw, $data);
                break;
            case 'application/json':
                $data = json_decode($raw, true);
                break;
        }

        return $data;
    }

    public function setRawBodyCallback($rawBodyCallback)
    {
        $this->_rawBodyCallback = $rawBodyCallback;

        return $this;
    }

    public function createHttpRequest()
    {
        $parent = parent::createHttpRequest();
        $reflect = new \ReflectionObject($parent);

        $properties = [];
        foreach ($reflect->getProperties() as $property) {
            $property->setAccessible(true);
            $properties[$property->getName()] = $property->getValue($parent);
        }

        return new Request($properties['url'], null, $properties['post'], $properties['files'], $properties['cookies'], $properties['headers'], $properties['method'], $properties['remoteAddr'], $properties['remoteHost'], ($this->_rawBodyCallback ?: [$this, 'rawBodyCallback']));
    }
}
