<?php

namespace OllyOllyOlly\Forms;

interface CsrfProviderInterface
{
    public function get();
    public function validate($csrf);
}
