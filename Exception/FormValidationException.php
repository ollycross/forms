<?php

namespace OllyOllyOlly\Forms\Exception;

class FormValidationException extends \OllyOllyOlly\Forms\Exception
{
    protected $_errors = [];

    public function __construct($errors = [], $message = null, $code = 0, \Exception $previous = null)
    {
        parent::__construct($message ?: 'The form is invalid', $code, $previous);
        $this->_errors = (array) $errors;
    }

    public function getErrors()
    {
        return $this->_errors;
    }
}
