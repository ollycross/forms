<?php

namespace OllyOllyOlly\Forms\Exception;

class FactoryNotInstanceOfFormFactoryException extends \OllyOllyOlly\Forms\Exception
{
}
