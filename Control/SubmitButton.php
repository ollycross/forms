<?php

namespace OllyOllyOlly\Forms\Control;

class SubmitButton extends Button
{
    public function __construct($caption = null)
    {
        parent::__construct($caption);
    }

    public function getControl($caption = null)
    {
        $control = parent::getControl($caption)
            ->removeAttribute('value')
            ->setText($this->translate($caption === null ? $this->caption : $caption))
            ->setName('button')
            ->setAttribute('type', 'submit');

        return $control;
    }
}
