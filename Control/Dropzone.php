<?php

namespace OllyOllyOlly\Forms\Control;

use Nette\Forms\Controls\BaseControl;
use Nette\Utils\Html;

class Dropzone extends BaseControl
{
    protected $_options;

    public function __construct($caption = null, $options = [], $message = 'Drag your files here to upload.')
    {
        parent::__construct($caption);
        $this->_options = $options;
        $this->control = Html::el('div')->setHtml("<div class=\"dz-message\" data-dz-message><span>{$message}</span></div>");
    }

    public function getControl()
    {
        $control = clone parent::getControl();
        $control->setAttribute('data-dropzone', json_encode($this->_options, JSON_FORCE_OBJECT));

        return $control;
    }
}
