<?php

namespace OllyOllyOlly\Forms\Control;

use Nette\Forms\Controls;

class Button extends Controls\Button
{
    public function __construct($caption = null)
    {
        parent::__construct($caption);
        $this->control
            ->removeAttribute('value')
            ->setText($this->translate($caption === null ? $this->caption : $caption))
            ->setName('button')
            ->setAttribute('type', 'button');
    }

    public function getControl($caption = null)
    {
        return $this->control;
    }
}
