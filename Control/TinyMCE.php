<?php

namespace OllyOllyOlly\Forms\Control;

use Nette\Forms\Controls\TextArea;

class TinyMCE extends TextArea
{
    protected $_options;

    public function __construct($caption = null, $options = [])
    {
        parent::__construct($caption);
        $this->_options = $options;
    }

    public function getControl()
    {
        $control = parent::getControl();
        $control->setAttribute('data-tinymce', json_encode($this->_options, JSON_FORCE_OBJECT));

        return $control;
    }
}
