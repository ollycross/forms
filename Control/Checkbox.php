<?php

namespace OllyOllyOlly\Forms\Control;

use Nette\Forms\Controls;
use Nette\Utils\Html;

class Checkbox extends Controls\Checkbox
{
    private $wrapper;

    public function __construct($label = null)
    {
        parent::__construct($label);
        $this->wrapper = Html::el();
    }

    public function getControl()
    {
        return $this->wrapper->insert(0, $this->getControlPart())->insert(1, $this->getLabelPart());
    }
}
