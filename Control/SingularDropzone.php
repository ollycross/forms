<?php

namespace OllyOllyOlly\Forms\Control;

use Nette\Utils\Html;

class SingularDropzone extends Dropzone
{
    protected $_name;
    protected $_existing;
    protected $_message;
    protected $_options;

    public function __construct($name, $caption = null, $options = [], $existing = null, $message = 'Drag your files here to upload.')
    {
        parent::__construct($caption);

        $this->_name = $name;
        $this->_options = $options;
        $this->_message = $message;
        $this->_existing = $existing;
        $this->control = Html::el('div')->setAttribute('class', 'singular-dropzone');
    }

    public function getControl()
    {
        $uniqid = uniqid();

        $control = Html::el('div')->setAttribute('class', 'singular-dropzone')
            ->setAttribute('id', $this->getHtmlId().'-'.$uniqid);

        $dzControl = Html::el('div')
            ->addAttributes([
                'class' => 'singular-dropzone-dropzone',
                'id' => $this->getHtmlId().'-dropzone-'.$uniqid,
                'data-dropzone' => json_encode($this->_options, JSON_FORCE_OBJECT),
            ])

            ->setHtml("<div class=\"dz-message\" data-dz-message><span>{$this->_message}</span></div>");

        $previewControl = Html::el('div')
            ->setAttribute('class', 'singular-dropzone-preview')
            ->addHtml(Html::el('img')->setAttribute('src', $this->_existing['url']))
            ->addHtml(Html::el('button')->addAttributes([
                'type' => 'button',
                'class' => 'btn btn-link',
            ])->setText('Change / Remove'));

        $idControl = Html::el('input')
            ->setAttribute('type', 'hidden')
            ->setAttribute('name', $this->_name)
            ->setAttribute('id', $this->getHtmlId())
            ->setAttribute('value', $this->_existing['id']);

        $control->addHtml($dzControl)->addHtml($previewControl)->addHtml($idControl);

        return $control;
    }
}
