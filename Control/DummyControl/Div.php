<?php

namespace OllyOllyOlly\Forms\Control\DummyControl;

use OllyOllyOlly\Forms\Control\DummyControl;

class Div extends DummyControl
{
    protected $_data = [];
    protected $_html = '';

    public function __construct($caption, $data, $html)
    {
        parent::__construct($caption);
        $this->_data = $data;
        $this->_html = $html;
    }

    public function getControl()
    {
        $control = clone $this->control;
        $control->setName('div');
        $control->setAttribute('id', $this->getHtmlId());
        $control->setHtml($this->_html);
        foreach ($this->_data as $property => $value) {
            $control->setAttribute("data-{$property}", $value);
        }

        return $control;
    }
}
