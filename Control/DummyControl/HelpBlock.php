<?php

namespace OllyOllyOlly\Forms\Control\DummyControl;

use OllyOllyOlly\Forms\Control\DummyControl;
use Nette\Utils\Html;

class HelpBlock extends DummyControl
{
    protected $_escapeCaption = true;
    public function __construct($caption = null, $escapeCaption = true)
    {
        $this->_escapeCaption = $escapeCaption;
        parent::__construct($caption);
    }

    public function getControl()
    {
        $control = Html::el('div', [
                'class' => 'help-block',
                'id' => $this->getHtmlId(),
            ]);
        $method = $this->_escapeCaption ? 'setText' : 'setHtml';
        call_user_func([$control, $method], $this->caption);

        return $control;
    }
}
