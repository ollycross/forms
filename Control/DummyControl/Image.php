<?php

namespace OllyOllyOlly\Forms\Control\DummyControl;

use OllyOllyOlly\Forms\Control\DummyControl;

class Image extends DummyControl
{
    protected $_url;
    public function __construct($url, $caption = null)
    {
        $this->_url = $url;
        parent::__construct($caption);
    }

    public function getControl()
    {
        $control = clone $this->control;

        $control->setName('div')
            ->setHtml("<img src=\"{$this->_url}\">");

        return $control;
    }
}
