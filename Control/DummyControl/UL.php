<?php

namespace OllyOllyOlly\Forms\Control\DummyControl;

use Nette\Forms\Controls\BaseControl;
use Nette\Utils\Html;

class UL extends BaseControl
{
    protected $_items;

    public function __construct($caption = null, $items = [])
    {
        $this->_items = $items;
        parent::__construct($caption);
    }

    public function addItem($item)
    {
        $this->_items[] = $item;

        return $this;
    }

    public function getControl()
    {
        $this->control = parent::getControl()
            ->setName('ul')
            ->appendAttribute('class', 'list-unstyled')
            ->setAttribute('id', $this->getHtmlId());
        foreach ($this->_items as $item) {
            $item = (object) $item;
            $li = Html::el('li')->setText($item->title);
            if ($item->id) {
                $li->setAttribute('data-id', $item->id);
            }
            foreach ($item->data ?? [] as $key => $value) {
                $li->setAttribute('data-'.$key, $value);
            }
            $this->control->addHtml($li);
        }

        return $this->control;
    }
}
