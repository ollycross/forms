<?php

namespace OllyOllyOlly\Forms\Control;

use Nette\Utils\Html;

class Taggle extends DummyControl
{
    protected $_options = [
    ];

    public function __construct($caption = null, $existing = [], $options = [])
    {
        parent::__construct($caption);
        $this->_options = array_replace($this->_options, $options);
        $this->_options['tags'] = $existing;
    }
    public function getControl()
    {
        $wrapper = Html::el('div');
        $control = parent::getControl();
        $control->setName('div');

        $name = $this->getHtmlName();

        $hidden = Html::el('input', [
            'type' => 'hidden',
            'name' => $name,
            'class' => 'hidden-input',
        ]);

        $this->_options['hiddenInputName'] = sprintf('%s[]', $name);
        $control
            ->setAttribute('data-taggle-options', json_encode($this->_options))
            ->appendAttribute('class', 'taggle clearfix form-control');

        $wrapper->insert(0, $control);
        $wrapper->insert(1, $hidden);

        return $wrapper;
    }

    public function setValue($value)
    {
        $this->value = $value;
        if (is_string($value)) {
            $this->_options['tags'] = json_decode($value);
        }

        return $this;
    }
}
