<?php

namespace OllyOllyOlly\Forms\Control;

use Nette\Utils\Html;

class TextInput extends \Nette\Forms\Controls\TextInput
{
    protected $_prepend;
    protected $_append;

    public function getPrepend()
    {
        return $this->_prepend;
    }

    public function setPrepend($prepend)
    {
        $this->_prepend = $prepend;

        return $this;
    }

    public function getAppend()
    {
        return $this->_append;
    }

    public function setAppend($append)
    {
        $this->_append = $append;

        return $this;
    }

    public function getControl()
    {
        $input = parent::getControl();
        $control = $input;
        if (($prepend = $this->getPrepend()) || ($append = $this->getAppend())) {
            $container = Html::el('div')
                ->setAttribute('class', 'input-group')
                ->insert(1, $input);
            if ($prepend) {
                $container->insert(0, Html::el('span')
                    ->setAttribute('class', 'input-group-addon')
                    ->setHtml($prepend));
            }
            if ($append) {
                $container->insert(2, Html::el('span')
                    ->setAttribute('class', 'input-group-addon')
                    ->setHtml($append));
            }
            $control = $container;
        }

        return $control;
    }
}
