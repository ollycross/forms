<?php

namespace OllyOllyOlly\Forms\Control;

use Nette\Forms\Controls;

class StarRating extends Controls\TextInput
{
    protected $_options = [
        'min' => 0,
        'max' => 5,
        'step' => 1,
        'caption' => false,
        'clear' => false,
    ];

    public function __construct($caption = null, $options = [])
    {
        parent::__construct($caption);
        $this->_options = array_replace($this->_options, $options);

        $this->control
            ->setAttribute('class', 'star-rating')
            ->setAttribute('data-min', $this->_options['min'])
            ->setAttribute('data-max', $this->_options['max'])
            ->setAttribute('data-step', $this->_options['step'])
            ->setAttribute('data-show-caption', $this->_options['caption'] ? 'true' : 'false')
            ->setAttribute('data-show-clear', $this->_options['clear'] ? 'true' : 'false')
            ;
    }
}
