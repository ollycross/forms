<?php

namespace OllyOllyOlly\Forms;

use OllyOllyOlly\Forms\Exception\FactoryNotInstanceOfFormFactoryException;
use OllyOllyOlly\Forms\Exception\FormNotFoundException;
use OllyOllyOlly\Forms\Exception\FormValidationException;
use OllyOllyOlly\Forms\Http\RequestFactory;

abstract class FormFactory
{
    protected $_name;

    protected $_form;

    protected static $csrfProvider;

    public static $factoryBase = '\OllyOllyOlly\Forms\FormFactory';

    public static $options = [
      'formClass' => '\OllyOllyOlly\Forms\Form',
    ];

    protected function __construct($slug)
    {
        $formClass = $this->getOption('formClass');
        $form = new $formClass($slug);
        $form->httpRequest = (new RequestFactory())->createHttpRequest();
        $this->_setForm($form);
    }

    abstract public function get();

    public static function create($slug)
    {
        $class = sprintf('%s\\%s', static::$factoryBase, Utilities::camelCase($slug, true));
        if (!class_exists($class)) {
            throw new FormNotFoundException("'{$slug}' not found (looked for '{$class}').");
        }
        $factory = new $class($slug);
        $required = __CLASS__;
        if (!$factory instanceof $required) {
            throw new FactoryNotInstanceOfFormFactoryException("'{$class}' must be an instance of ".__CLASS__);
        }

        $form = call_user_func_array([$factory, 'get'], array_slice(func_get_args(), 1));
        if (!$form->getName()) {
            $form->setName($factory->getName());
        }

        if (!$form->hasSubmit() && !$factory::getOption('allow-no-submit')) {
            $form->addSubmit('submit', 'Submit');
        }

        $form->getElementPrototype()->appendAttribute('class', 'netteform');

        if (static::$csrfProvider) {
            $form->addHidden('csrf')
                ->setRequired(true)
                ->setAttribute('id', 'frm-csrf')
                ->addRule(get_called_class().'::validateCsrf', 'Token expired; please submit the form again.')
                ;

            if (!$form->isSubmitted()) {
                static::refreshCsrf($form);
            }
        }

        $form->onError[] = get_called_class().'::refreshCsrf';
        foreach (['onSuccess', 'onError'] as $event) {
            $callable = [$factory, $event];
            if (is_callable($callable)) {
                $form->{$event}[] = $callable;
            }
        }
        $form->onError[] = get_called_class().'::throwErrorException';

        return $form;
    }

    public static function throwErrorException($form)
    {
        $errors = ['form' => $form->getErrors()];
        foreach ($form->getComponents() as $control) {
            if ($control->getErrors()) {
                $errors[] = [
                    'element' => '#'.$control->getHtmlId(),
                    'message' => $control->getErrors(),
                ];
            }
        }
        throw new FormValidationException(array_filter($errors));
    }

    // public static function create(...$args)
    // {
    //     array_unshift($args, get_called_class());

    //     return call_user_func_array([get_called_class(), 'createNamespaced'], $args);
    // }
    public static function getOption($key)
    {
        $options = array_replace(self::$options, static::$options);

        return isset($options[$key]) ? $options[$key] : null;
    }

    public static function setCsrfProvider(CsrfProviderInterface $csrfProvider)
    {
        static::$csrfProvider = $csrfProvider;
    }

    public static function refreshCsrf(Form $form)
    {
        $form['csrf']->setValue(static::$csrfProvider->get());
    }

    public static function validateCsrf($csrf)
    {
        return static::$csrfProvider->validate($csrf->value);
    }

    public function getName()
    {
        return $this->_name ?: Utilities::snakeCase(Utilities::classBasename(get_called_class()));
    }
    protected function _autoDefaults($entity)
    {
        if (!is_array($entity)) {
            $entity = $entity->cast();
        }
        foreach ($this->getForm()->getComponents() as $key => $component) {
            if (isset($entity[$key]) && $entity[$key] && !$component->getValue()) {
                $component->setDefaultValue($entity[$key]);
            }
        }
    }

    protected function _autoValues($available, $writable)
    {
        $out = array_filter((array) $available, function ($value, $key) use ($writable) {
            return isset($writable[$key])/* && ($value)*/;
        }, ARRAY_FILTER_USE_BOTH);

        return $out;
    }

    /**
     * Gets the value of _form.
     *
     * @return mixed
     */
    public function getForm()
    {
        return $this->_form;
    }

    /**
     * Sets the value of _form.
     *
     * @param mixed $_form the form
     *
     * @return self
     */
    protected function _setForm(Form $form)
    {
        $this->_form = $form;

        return $this;
    }
}
