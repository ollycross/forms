<?php

namespace OllyOllyOlly\Forms;

class Utilities
{
    public static function classBasename($classOrObject)
    {
        $reflect = new \ReflectionClass($classOrObject);

        return $reflect->getShortname();
    }
    public static function camelCase($str, $ucfirst = false)
    {
        $str = preg_replace('/[^a-z0-9]+/i', ' ', $str);
        $str = trim($str);
        $str = ucwords($str);
        $str = str_replace(' ', '', $str);
        if (!$ucfirst) {
            $str = lcfirst($str);
        }

        return $str;
    }

    public static function snakeCase($str)
    {
        return strtolower(preg_replace(array('/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'), '$1_$2', $str));
    }
}
