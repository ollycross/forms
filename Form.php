<?php

namespace OllyOllyOlly\Forms;

use OllyOllyOlly\Forms\Http\RequestFactory;
use Nette\Forms;
use OllyOllyOlly\Forms\Control\Checkbox;
use OllyOllyOlly\Forms\Control\DummyControl\Div;
use OllyOllyOlly\Forms\Control\DummyControl\HelpBlock;
use OllyOllyOlly\Forms\Control\DummyControl\Image;
use OllyOllyOlly\Forms\Control\DummyControl\UL;
use OllyOllyOlly\Forms\Control\StarRating;
use OllyOllyOlly\Forms\Control\Taggle;
use OllyOllyOlly\Forms\Control\TextInput;

class Form extends Forms\Form
{
    protected $_rows = [];
    protected $_columns = [];

    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->setRenderer(new BootstrapFormRenderer());
    }

    public function has($class)
    {
        $bases = ['Nette\Forms\Controls\\', 'OllyOllyOlly\Forms\Control\\'];
        foreach ($this->getComponents() as $component) {
            foreach ($bases as $base) {
                if ($component instanceof $base.$class) {
                    return true;
                }
            }
        }

        return false;
    }
    public function hasSubmit()
    {
        return $this->has('SubmitButton');
    }
    public function hasButton()
    {
        return $this->has('Button');
    }

    protected function _getGIx()
    {
        $currentGroup = $this->getCurrentGroup();

        return $currentGroup ? array_search($this->getCurrentGroup(), array_values($this->getGroups())) + 1 : 0;
    }

    public function startRow()
    {
        $gIx = $this->_getGIx();
        $this->_rows[$gIx] = true;
    }

    public function startColumn($span = 6)
    {
        $gIx = $this->_getGIx();
        $this->_columns[$gIx] = $span;
    }
    public function getColumns()
    {
        return $this->_columns;
    }
    public function getRows()
    {
        return $this->_rows;
    }

    public function addCheckbox($name, $caption = null)
    {
        return $this[$name] = new Checkbox($caption);
    }
    public function addHelpBlock($name, $caption = null, $escapeCaption = true)
    {
        return $this[$name] = new HelpBlock($caption, $escapeCaption);
    }
    public function addPicture($name, $src, $caption = null)
    {
        return $this[$name] = new Image($src, $caption);
    }

    public function addStarRating($name, $caption = null, $options = [])
    {
        return $this[$name] = new StarRating($caption, $options);
    }

    public function addSubmit($name, $caption = null)
    {
        return $this[$name] = new Control\SubmitButton($caption);
    }
    public function addButton($name, $caption = null)
    {
        return $this[$name] = new Control\Button($caption);
    }
    public function addTinyMCE($name, $caption = null, $options = [])
    {
        return $this[$name] = new Control\TinyMCE($caption, $options);
    }
    public function addDropzone($name, $caption = null, $options = [], $message = null)
    {
        return $this[$name] = new Control\Dropzone($caption, $options, $message);
    }
    public function addSingularDropzone($name, $caption = null, $options = [], $existing = null, $message = null)
    {
        return $this[$name] = new Control\SingularDropzone($name, $caption, $options, $existing, $message);
    }
    public function addUL($name, $items = [], $caption = null)
    {
        return $this[$name] = new UL($caption, $items);
    }

    public function addDiv($name, $data = [], $caption = null, $html = null)
    {
        return $this[$name] = new Div($caption, $data, $html);
    }

    public function addTaggle($name, $caption = null, $existing = [], $options = [])
    {
        return $this[$name] = new Taggle($caption, $existing, $options);
    }

    public function addText($name, $label = null, $cols = null, $maxLength = null)
    {
        return $this[$name] = (new TextInput($label, $maxLength))
            ->setHtmlAttribute('size', $cols);
    }

    private function getHttpRequest()
    {
        if (!$this->httpRequest) {
            $factory = new RequestFactory();
            $this->httpRequest = $factory->createHttpRequest();
        }

        return $this->httpRequest;
    }

    /**
     * Internal: returns submitted HTTP data or NULL when form was not submitted.
     *
     * @return array|null
     */
    protected function receiveHttpData()
    {
        $data = parent::receiveHttpData();
        if (!$data) {
            $httpRequest = $this->getHttpRequest();
            $data = $httpRequest->getRawBody();
        }

        return $data;
    }
}
